/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.algorithmshomework;

import java.util.Scanner;
import static javax.swing.UIManager.get;

/**
 *
 * @author User
 */
class MemberHash {

    private int currentSize, maxSize;
    private String[] keys;
    private String[] vals;

    public MemberHash(int capacity) {
        currentSize = 0;
        maxSize = capacity;
        keys = new String[maxSize];
        vals = new String[maxSize];

    }

    public void makeEmpty() {
        currentSize = 0;
        keys = new String[maxSize];
        vals = new String[maxSize];
    }

    public int getSize() {
        return currentSize;
    }

    public boolean isFull() {
        return currentSize == maxSize;
    }

    public boolean contains(String key) {
        return get(key) != null;
    }

    private int hash(String key) {
        return key.hashCode() % maxSize;
    }

    public void insert(String key, String val) {
        int tmp = hash(key);
        int i = tmp;

        do {
            if (keys[i] == null) {
                keys[i] = key;
                vals[i] = val;
                currentSize++;
                return;
            }

            if (keys[i].equals(key)) {
                vals[i] = val;
                return;
            }

            i = (i + 1) % maxSize;

        } while (i != tmp);
    }
    public void printHashTable()
    {
        System.out.println("\nHash Table: ");
        for (int i = 0; i < maxSize; i++)
            if (keys[i] != null)
                System.out.println(keys[i] + " " + vals[i]);
        System.out.println();
    }
}

public class Hashing {

    public static void main(String[] args) {
        Scanner kb = new Scanner(System.in);
        System.out.println("Hash Table");
        System.out.println("Enter size");
         MemberHash  MemberHash
            = new  MemberHash(kb.nextInt());
        System.out.println("Enter key and value");
                MemberHash.insert(kb.next(), kb.next());
                  MemberHash.printHashTable();
    }
}
